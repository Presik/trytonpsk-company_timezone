# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from dateutil import tz
from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    @classmethod
    def convert_timezone(cls, date_time, to_utc=False, company_id=None):
        if not company_id:
            company_id = Transaction().context.get('company')

        company = cls(company_id)

        if company.timezone:
            if to_utc:
                from_zone = tz.gettz(company.timezone)
                to_zone = tz.gettz('UTC')
            else:
                from_zone = tz.gettz('UTC')
                to_zone = tz.gettz(company.timezone)
            date_time = date_time.replace(tzinfo=from_zone)
            return date_time.astimezone(to_zone)
        else:
            return date_time
