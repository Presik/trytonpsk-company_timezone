#This file is part company_logo module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.pool import Pool
from .company import Company

def register():
    Pool.register(
        Company,
        module='company_timezone', type_='model')
